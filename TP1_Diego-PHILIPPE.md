# TP1 :

 Adresse réseau :  192.168.0.0/23                                                                       
|     Utilisation     |   Premier   |     Dernier    |  Statique ou Dynamic |
|---------------------|-------------|----------------|----------------------|
| Périphériques Hôtes |192.168.0.1  |192.168.1.194   |      dynamique       |
| Serveurs            |192.168.1.195|192.168.1.224   |       static         |
| Imprimantes         |192.168.1.225|192.168.1.239   |       static         |
| Périphériques Int.  |192.168.1.240|192.168.1.253   |       static         |
| Passerelle          |192.168.1.254|                |       static         |

Bonus : Je pense que le fait que tout les équipement puissent communiquer entre eux peut-être dangeureux, parce que si le réseau est attaqué ou compromis le danger s'applique à tous les équipements.

